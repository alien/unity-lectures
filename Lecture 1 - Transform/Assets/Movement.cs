﻿using UnityEngine;
using System.Collections;

public class Movement : MonoBehaviour {

    public float speed_t, speed_r, speed_s;
	public GameObject other;

	// Use this for initialization
	void Start () {
		//Don't forget to disable VSync
		//Application.targetFrameRate = 10;
	}
	
	// Update is called once per frame
	void Update () {
		/*
		 * Translation 
		 */
		float x_axis = Input.GetAxis ("Horizontal");
		float z_axis = Input.GetAxis ("Vertical");
		//transform.Translate (x_axis, 0, 0);
		//transform.Translate (0, 0, z_axis);
		//What's wrong? - Framerate dependent
		//transform.Translate (x_axis*Time.deltaTime*speed_t, 0, 0);
		//transform.Translate (0, 0, z_axis*Time.deltaTime*speed_t);
        //What's wrong? - Straferunning ;)
		Vector3 u = Vector3.Normalize (new Vector3 (x_axis, 0, z_axis));
		transform.Translate (u.x*Time.deltaTime*speed_t, 0, 0);
		transform.Translate (0, 0, u.z*Time.deltaTime*speed_t);
		/*
		 * Rotation 
		 */
        float mouse_x = Input.GetAxis ("Mouse X");
        float mouse_y = Input.GetAxis ("Mouse Y");
		transform.GetChild (0).Rotate (mouse_y * Time.deltaTime * speed_r, 0, 0, Space.Self);
		transform.Rotate (0, mouse_x * Time.deltaTime * speed_r, 0, Space.World);
		/*
		 * Scaling
		 */
		other.transform.localScale += Vector3.one * Time.deltaTime * speed_s;
    }
}
