﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HUD : MonoBehaviour {

	public Text elapsedTime;

	// Update is called once per frame
	void Update () {
	
		elapsedTime.text = "Elapsed Time: " + Time.time.ToString ();
	}
}
