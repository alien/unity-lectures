using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class AppManagerScript : MonoBehaviour {

	public void QuitGame ()
	{
		Application.Quit();
	}

	public void StartGame ()
	{
		SceneManager.LoadScene ("main");
	}
}
